const rdf = require('rdf')
const {getAllProducts,getProduct} = require('./esHelper');
const { writeFileSync, appendFileSync } = require('fs');
const obj2Rdf = require('./obj2rdf');

const outputIdFilePath = './ids/products.json'
const outputFilePath = "./data/products.rdf"
const Prefix = "Product"
const getInfo = getProduct
const getAllIds = getAllProducts

async function json2Rdf(id){
  const info = await getInfo(id)
  const result = [];

  const blankSubject = `${Prefix}-${id}`;

  result.push(`<${blankSubject}> <dgraph.type> "${Prefix}" .`);
  result.push(`<${blankSubject}> <${Prefix}.xid> "${id}" .`);

  result.push(...obj2Rdf(info,blankSubject,Prefix,null,['id','translations','timestamps','brandCode']));

  if(info.brandCode){
    result.push(`<${blankSubject}> <${Prefix}.brand> <Brand-${info.brandCode}> .`);
  }
  
  for(let trans in info.translations){
      let lang = trans.substring(0,2);
      let item = info.translations[trans];
      result.push(...obj2Rdf(item,blankSubject,Prefix,lang,['languageCode']))
  }

  appendFileSync(outputFilePath,result.join('\n')+'\n\n') ;
}

async function getIds(){
  if(existsSync(outputIdFilePath)){
      const str= readFileSync(outputIdFilePath)
      return JSON.parse(str);
  }else{
      const ids = await getAllIds();
      writeFileSync(outputIdFilePath,JSON.stringify(ids));
      return ids;
  }

}

async function main(){
    const ids = await getIds();
    writeFileSync(outputFilePath,`# start at ${new Date} \n`)
    let index =0;
    let length = ids.length;
    for(let id of ids){
        if(index++%50===0){
            console.log(`start ${index}/${length}`)
        }
        await json2Rdf(id)
    }
    appendFileSync(outputFilePath,`# end at ${new Date} \n`)
}

main()
.then(()=>console.log('finished'))
.catch(err=>console.error(err))

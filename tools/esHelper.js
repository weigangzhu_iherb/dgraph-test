const { Client } = require('@elastic/elasticsearch')

const client = new Client({
  node: 'https://elastic-catalog.app.iherbpreprod.com',
  // maxRetries: 5,
  // requestTimeout: 60000,
  // sniffOnStart: true
})

async function getAllData(index) {
  
  let response = await client.search({
    index,
    scroll: '10m',
    size: 1000,
    _source:false
  });

  const total = response.body.hits.total.value;
  const data = [];
  while (true) {
    console.log(`${data.length}/${total}`);

    if(response.statusCode!==200){
      console.error('response error')
      break;
    }
    const sourceHits = response.body.hits.hits.map(t=>t._id)

    if (sourceHits.length === 0) {
      break
    }

    data.push(...sourceHits);

    if (!response.body._scroll_id) {
      break
    }

    try{
      response = await client.scroll({
        scrollId: response.body._scroll_id,
        scroll: "10m"
      })
    }catch(err){
      console.error(err)
    }
    
  }

  return data;
}

async function getDocument(index,id){
  const result = await client.get({id,index});
  return result.body._source;
}

async function getAllProducts() {
  return getAllData('products')
}

async function getAllBrands() {
  return getAllData('brands')
}

async function getProduct(id){
  return getDocument('products',id)
}

async function getBrand(id){
  return getDocument('brands',id)
}



if (require.main === module) {
  // getAllBrands()
  // .then(result=>console.log(result))
  // .catch(err=>console.error(err))

  getProduct(12121)
  .then((result)=>console.log(result))
  .catch(err=>console.error(err))
}
else{
  module.exports = {
    getAllData,
    getDocument,
    getAllProducts,
    getProduct,
    getAllBrands,
    getBrand
  }
}
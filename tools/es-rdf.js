const axios = require('axios');
const productIds = require('./data.json')
const rdf = require('rdf')
const { writeFileSync, appendFileSync } = require('fs');

async function getProduct(id){
    const resp = await axios.get(`https://elastic-catalog.app.iherbpreprod.com/products/_doc/${id}`);     
    return resp.data._source;
}
function formatValue(value){
    if(typeof value ==='string'){
        return new rdf.Literal(value).toNT()
    }
    return `"${value}"`;
}
async function json2Rdf(id){
    const product = await getProduct(id)
    const result = [];

    result.push(`<product-${product.id}> <dgraph.type> "Product" .`);
    result.push(`<product-${product.id}> <Product.xid> "${product.id}" .`);
    for(let p in product){
        const v = product[p];
        if(p ==='id' || typeof v ==='object'){
            continue
        }
        result.push(`<product-${product.id}> <Product.${p}> ${formatValue(product[p])} .`);
    }
    
    for(let trans in product.translations){
        let lang = trans.substring(0,2);
        let item = product.translations[trans];
        for(let trans_detail in item){
            if(['languageCode'].indexOf(trans_detail)>-1)
                continue;

            let value = item[trans_detail];
            
            
            result.push(`<product-${product.id}> <Product.${trans_detail}> ${formatValue(value)}@${lang} .`);
        }
    }

    product.storeIds.forEach(item=>{
        result.push(`<product-${product.id}> <Product.storeIds> "${item}" .`);
    })

    if(product.images){
        result.push(`<product-${product.id}> <Product.images.isThreeSixtyEnabled> "${product.images.isThreeSixtyEnabled}" .`);
        product.images.threeSixty.forEach((item)=>{
            result.push(`<product-${product.id}> <Product.images.threeSixty> "${item}" .`);
        })
        product.images.list.forEach((item)=>{
            result.push(`<product-${product.id}> <Product.images.list> "${item}" .`);
        })
        if(product.images.primary){
            result.push(`<product-${product.id}> <Product.images.primary> "${product.images.primary}" .`);
        }
    }
    

    appendFileSync(outputFilePath,result.join('\n')+'\n\n') ;
    
    
}

const outputFilePath = "./data/products.rdf"


async function main(){
    writeFileSync(outputFilePath,`# start at ${new Date} \n`)
    let index =0;
    let length = productIds.length;
    for(let id of productIds){
        if(index++%50===0){
            console.log(`start ${index}/${length}`)
        }
        await json2Rdf(id)
    }
    appendFileSync(outputFilePath,`# end at ${new Date} \n`)
}

main()
.then(()=>console.log('finished'))
.catch(err=>console.error(err))

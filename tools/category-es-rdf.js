const rdf = require('rdf')
const {getAllData,getDocument} = require('./esHelper');
const { writeFileSync, appendFileSync, fstat, existsSync, readFileSync } = require('fs');
const obj2Rdf = require('./obj2rdf');

const outputIdFilePath = './ids/categories.json';
const outputFilePath = "./data/categories.rdf"
const Prefix = "Category"
const getInfo = (id)=> getDocument('categories',id)
const getAllIds = ()=>getAllData('categories')

async function json2Rdf(id){
  const info = await getInfo(id)
  const result = [];

  const blankSubject = `${Prefix}-${id}`;

  result.push(`<${blankSubject}> <dgraph.type> "${Prefix}" .`);
  result.push(`<${blankSubject}> <${Prefix}.xid> "${id}" .`);

  result.push(...obj2Rdf(info,blankSubject,Prefix,null,['id','translations','timestamps','parentCategoryId','brandCode','hierarchy']));

  if(info.parentCategoryId){
    result.push(`<${blankSubject}> <${Prefix}.parentCategoryId> <${Prefix}-${info.parentCategoryId}> .`);
  }

  if(info.brandCode){
    result.push(`<${blankSubject}> <${Prefix}.brand> <Brand-${info.brandCode}> .`);
  }
  
  if(info.translations){
    for(let trans in info.translations){
        let lang = trans.substring(0,2);
        let item = info.translations[trans];
        result.push(...obj2Rdf(item,blankSubject,Prefix,lang,['languageCode','id','banners']))
        if(item.banners){
            if(item.banners.desktop && item.banners.desktop.length>0){
                let subSubject = `${blankSubject}-banners-desktop`
                result.push(`<${subSubject}> <dgraph.type> "CategoryBanners" .`);
                result.push(`<${subSubject}> <CategoryBanners.xid> "${subSubject}" .`);
                item.banners.desktop.forEach((subItem)=>{
                    result.push(...obj2Rdf(subItem,subSubject,"CategoryBanners",lang))
                })
                result.push(`<${blankSubject}> <${Prefix}.banners.desktop> <${subSubject}> .`);
            }

            if(item.banners.mobile && item.banners.mobile.length>0){
                let subSubject = `${blankSubject}-banners-mobile`
                result.push(`<${subSubject}> <dgraph.type> "CategoryBanners" .`);
                result.push(`<${subSubject}> <CategoryBanners.xid> "${subSubject}" .`);
                item.banners.desktop.forEach((subItem)=>{
                    result.push(...obj2Rdf(subItem,subSubject,"CategoryBanners",lang))
                })
                
                result.push(`<${blankSubject}> <${Prefix}.banners.mobile> <${subSubject}> .`);
            }
        }
    }
  }

  appendFileSync(outputFilePath,result.join('\n')+'\n\n') ;
}

async function getIds(){
    if(existsSync(outputIdFilePath)){
        const str= readFileSync(outputIdFilePath)
        return JSON.parse(str);
    }else{
        const ids = await getAllIds();
        writeFileSync(outputIdFilePath,JSON.stringify(ids));
        return ids;
    }
}

async function main(){
    const ids = await getIds();
    writeFileSync(outputFilePath,`# start at ${new Date} \n`)
    let index =0;
    let length = ids.length;
    for(let id of ids){
        if(index++%50===0){
            console.log(`start ${index}/${length}`)
        }
        await json2Rdf(id)
    }
    appendFileSync(outputFilePath,`# end at ${new Date} \n`)
}

main()
.then(()=>console.log('finished'))
.catch(err=>console.error(err))

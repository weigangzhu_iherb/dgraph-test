const rdf = require('rdf')

function formatValue(value){
  if(typeof value ==='string'){
      return new rdf.Literal(value).toNT()
  }
  return `"${value}"`;
}

function formatRdf(s,p,o,lang){
  return `<${s}> <${p}> ${o}${lang?'@'+lang:''} .` 
}

function isNotEmptyValue(value){
  if(typeof value ==='string'){
    if(value){
      return true;
    }
    return false
  }
  if(typeof value ==='number'){
    if(`${value}`===''){
      return false;
    }
    return true;
  }

  return true
}

function obj2Rdf(obj,subject,predicate,lang,ignoreFields){
  const result = []
  for(let p in obj){
    if(ignoreFields && ignoreFields.indexOf(p)>-1){
      continue
    }
    const v = obj[p];
    if(!isNotEmptyValue(v)){
      continue
    }

    if(typeof v === 'object'){
      if(Array.isArray(v)){
        v.forEach((item,i)=>{
          if(typeof item ==='object'){
            const newPredicate = predicate+p[0].toUpperCase()+p.substring(1)
            const itemSubject = `${subject}-${p}-${i}`
            result.push(`<${itemSubject}> <dgraph.type> "${newPredicate}" .`);
            result.push(`<${itemSubject}> <${newPredicate}.xid> "${itemSubject}" .`);
            
            result.push(...obj2Rdf(item,itemSubject,newPredicate,lang,ignoreFields))
            result.push(formatRdf(subject,predicate+'.'+p,`<${itemSubject}>`,lang))
          }else{
            result.push(formatRdf(subject,predicate+'.'+p,formatValue(item),lang))
          }
        })
      }else{
        result.push(...obj2Rdf(v,subject,predicate+'.'+p,lang,ignoreFields))
      }
    }else{
      result.push(formatRdf(subject,predicate+'.'+p,formatValue(v),lang))
    }
  }
  return result;
}

if(require.main === module){
  const info = {
    id:1,
    user:{
      id:1
    },
    addr:[
      {line1:"1",line2:"1-2"},
      {line1:"2",line2:"2-2"},
    ],
    storeids:[
      1,
      2
    ]
  }
  console.log(obj2Rdf(info,'product-1','product'));
}else{
  module.exports = obj2Rdf
}
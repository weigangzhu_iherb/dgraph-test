const dgraph = require("dgraph-js");
// const grpc = require("./../node_modules/grpc/src/");

const clientStub = new dgraph.DgraphClientStub(
  // addr: optional, default: "localhost:9080"
  "localhost:9080"
  // credentials: optional, default: grpc.credentials.createInsecure()
  // grpc.credentials.createInsecure(),
);
const dgraphClient = new dgraph.DgraphClient(clientStub);

async function main (){
  const query = `{
    all(func: has(Category.xid))
    {
      Category.xid
      Category.name
    }
  }`;

  const res = await dgraphClient.newTxn().query(query);
  const categories = res.getJson();
  
  // Print results.
  console.log(categories)
}

main().then(()=>console.log('finished'))
.catch((err)=>console.error(err));